package com.todo.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.todo.app.entity.Todo;
import com.todo.app.service.TodoService;

@Controller
public class TodoController {

	public boolean flg = true;
    
    @Autowired
    TodoService todoService;

    @RequestMapping(value="/")
    public String index(Model model) {
    	List<Todo> list = todoService.selectIncomplete();
    	List<Todo> doneList = todoService.selectComplete();
        model.addAttribute("todos",list);
        model.addAttribute("doneTodos",doneList);
        return "index";	
    }
    
    @RequestMapping(value="/add")
    public String add(Todo todo) {
    	if(todo.getTitle() == "") {
    		return "errorPage";
    	} else {
    		todoService.add(todo);
    	}
    	return "redirect:/";
    }

    @RequestMapping(value="/update")
    @ResponseBody
    public void update(Todo todo) {
    	this.flg = true;
    	try {
    		todoService.update(todo);
    	}catch(Exception e) {
    		this.flg = false;
    	}
    }

    @RequestMapping(value="/delete")
    @ResponseBody
    public void delete() {
    	this.flg = true;
    	try {
    		todoService.delete();
    	}catch(Exception e) {
    		this.flg = false;
    	}
    }
    
    @PostMapping(value="/returnIndex")
    public String returnIndex() {
        return "redirect:/";
    }

}