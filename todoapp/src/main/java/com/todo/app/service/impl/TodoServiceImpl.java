package com.todo.app.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.todo.app.entity.Todo;
import com.todo.app.mapper.TodoMapper;
import com.todo.app.service.TodoService;

@Service
public class TodoServiceImpl implements TodoService{

    private TodoMapper todoMapper;
    
    public TodoServiceImpl(TodoMapper mapper) {
        this.todoMapper = mapper;
      }
	
    @Override
    public List<Todo> selectIncomplete(){
        List<Todo> list = todoMapper.selectIncomplete();
    	return list;
    }
    
    @Override
    public List<Todo> selectComplete(){
        List<Todo> doneList = todoMapper.selectComplete();
    	return doneList;
    }
    
    @Override
    public boolean add(Todo todo) {
    	boolean ret = true;
    	try {
    		todoMapper.add(todo);
    	}catch(Exception e) {
    		ret = false;
    	}
    	return ret;
    }
    
    @Override
    public boolean update(Todo todo) {
    	boolean ret = true;
    	try {
    		todoMapper. update(todo);
    	}catch(Exception e) {
    		ret = false;
    	}
    	return ret;
    }
    
    @Override
    public boolean delete() {
    	boolean ret = true;
    	try {
    		todoMapper. delete();
    	}catch(Exception e) {
    		ret = false;
    	}    	
    	return ret;
    }
}
