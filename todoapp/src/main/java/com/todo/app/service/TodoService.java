package com.todo.app.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.todo.app.entity.Todo;

@Service
public interface TodoService {

	public List<Todo> selectIncomplete();
	
	public List<Todo> selectComplete();
	
	public boolean add(Todo todo);
	
	public boolean update(Todo todo);
	
	public boolean delete();
}
