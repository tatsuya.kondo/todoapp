package com.todo.app.service.impl;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.todo.app.entity.Todo;
import com.todo.app.mapper.TodoMapper;

public class TodoServiceImplTest {

	@Mock
	private TodoMapper todoMapper;

	@InjectMocks
	private TodoServiceImpl todoService;
	
	@Captor
	ArgumentCaptor<Todo> todoCaptor = ArgumentCaptor.forClass(Todo.class);

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		when(todoMapper.selectIncomplete()).thenReturn(getSelectIncomplete());
		when(todoMapper.selectComplete()).thenReturn(getSelectComplete());
	}

	@Test
	public void selectIncomplete() {
		// テスト対象のメソッドを実行
		List<Todo> todoList = todoService.selectIncomplete();
		// テスト対象の戻り値を検証
		assertEquals(1, todoList.size());
		assertEquals(1, todoList.get(0).getId());
		assertEquals("タスク1", todoList.get(0).getTitle());
		assertEquals(0, todoList.get(0).getDone_flg());
		assertEquals("2022-01-01", todoList.get(0).getTime_limit());
		//回数検証
		verify(todoMapper, times(0)).selectAll();
		verify(todoMapper, times(1)).selectIncomplete();
		verify(todoMapper, times(0)).selectComplete();
		verify(todoMapper, times(0)).add(Mockito.any());
		verify(todoMapper, times(0)).update(Mockito.any());
		verify(todoMapper, times(0)).delete();

	}

	@Test
	public void selectComplete() {
		// テスト対象のメソッドを実行
		List<Todo> todoList = todoService.selectComplete();
		// テスト対象の戻り値を検証
		assertEquals(1, todoList.size());
		assertEquals(2, todoList.get(0).getId());
		assertEquals("タスク2", todoList.get(0).getTitle());
		assertEquals(1, todoList.get(0).getDone_flg());
		assertEquals("2022-02-02", todoList.get(0).getTime_limit());
		//回数検証
		verify(todoMapper, times(0)).selectAll();
		verify(todoMapper, times(0)).selectIncomplete();
		verify(todoMapper, times(1)).selectComplete();
		verify(todoMapper, times(0)).add(Mockito.any());
		verify(todoMapper, times(0)).update(Mockito.any());
		verify(todoMapper, times(0)).delete();
	}
	
	@Test
	public void add() {
		//リクエスト情報の設定
		Todo request = new Todo();
		request.setId(1);
		request.setTitle("リクエスト情報");
		request.setDone_flg(0);
		request.setTime_limit("2023-01-01");
		// テスト対象のメソッドを実行
		boolean response = todoService.add(request);
		// テスト対象の戻り値を検証
		assertEquals(true, response);
		//回数検証
		verify(todoMapper, times(0)).selectAll();
		verify(todoMapper, times(0)).selectIncomplete();
		verify(todoMapper, times(0)).selectComplete();
		verify(todoMapper, times(1)).add(todoCaptor.capture());
		Todo todo = todoCaptor.getValue();
		assertEquals(todo,request);
		verify(todoMapper, times(0)).update(Mockito.any());
		verify(todoMapper, times(0)).delete();
	}
	
	@Test
	public void update() {
		//リクエスト情報の設定
		Todo addRequest = new Todo();
		addRequest.setId(1);
		addRequest.setTitle("リクエスト情報");
		addRequest.setDone_flg(0);
		addRequest.setTime_limit("2023-01-01");
		Todo updateRequest = new Todo();
		updateRequest.setId(1);
		updateRequest.setTitle("リクエスト");
		updateRequest.setDone_flg(1);
		updateRequest.setTime_limit("2035-01-01");
		// テスト対象のメソッドを実行
		boolean addResponse = todoService.add(addRequest);
		boolean updateResponse = todoService.update(updateRequest);
		// テスト対象の戻り値を検証
		assertEquals(true, addResponse);
		assertEquals(true, updateResponse);
		//回数検証
		verify(todoMapper, times(0)).selectAll();
		verify(todoMapper, times(0)).selectIncomplete();
		verify(todoMapper, times(0)).selectComplete();
		verify(todoMapper, times(1)).add(todoCaptor.capture());
		List <Todo> todo = todoCaptor.getAllValues();
		assertEquals(todo.get(0),addRequest);
		verify(todoMapper, times(1)).update(todoCaptor.capture());
		List<Todo> todoList = todoCaptor.getAllValues();
		assertEquals(todoList.get(1),updateRequest);
		verify(todoMapper, times(0)).delete();
	}
	
	@Test
	public void delete() {
		//リクエスト情報の設定
		Todo addRequest = new Todo();
		addRequest.setId(1);
		addRequest.setTitle("リクエスト情報");
		addRequest.setDone_flg(1);
		addRequest.setTime_limit("2023-01-01");
		// テスト対象のメソッドを実行
		boolean addResponse = todoService.add(addRequest);
		boolean deleteResponse = todoService.delete();
		// テスト対象の戻り値を検証
		assertEquals(true, addResponse);
		assertEquals(true, deleteResponse);
		//回数検証
		verify(todoMapper, times(0)).selectAll();
		verify(todoMapper, times(0)).selectIncomplete();
		verify(todoMapper, times(0)).selectComplete();
		verify(todoMapper, times(1)).add(todoCaptor.capture());
		List <Todo> todo = todoCaptor.getAllValues();
		assertEquals(todo.get(0),addRequest);
		verify(todoMapper, times(0)).update(Mockito.any());
		verify(todoMapper, times(1)).delete();
	}

	/**
	 * selectImcompleteのレスポンス
	 * 
	 * @return List<Todo>
	 */
	List<Todo> getSelectIncomplete() {
		List<Todo> todoExpect = new ArrayList<Todo>();
		Todo todo = new Todo();
		todo.setId(1);
		todo.setTitle("タスク1");
		todo.setDone_flg(0);
		todo.setTime_limit("2022-01-01");
		todoExpect.add(todo);
		return todoExpect;
	}

	/**
	 * selectCompleteのレスポンス
	 * 
	 * @return List<Todo>
	 */
	List<Todo> getSelectComplete() {
		List<Todo> todoExpect = new ArrayList<Todo>();
		Todo todo = new Todo();
		todo.setId(2);
		todo.setTitle("タスク2");
		todo.setDone_flg(1);
		todo.setTime_limit("2022-02-02");
		todoExpect.add(todo);
		return todoExpect;
	}
}
