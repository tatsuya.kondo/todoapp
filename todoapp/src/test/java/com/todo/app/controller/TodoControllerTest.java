package com.todo.app.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;

import com.todo.app.entity.Todo;
import com.todo.app.service.TodoService;

public class TodoControllerTest {

	@Mock
	private TodoService todoService;

	@InjectMocks
	private TodoController todoController;
	
	@Captor
	ArgumentCaptor<Todo> todoCaptor = ArgumentCaptor.forClass(Todo.class);

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		//モック設定
		when(todoService.selectIncomplete()).thenReturn(getSelectIncomplete());
		when(todoService.selectComplete()).thenReturn(getSelectComplete());
	}

	@Test
	public void index() {
		Model model = mock(Model.class);
		// テスト対象のメソッドを実行
		String indexResponse = todoController.index(model);
		// テスト対象の戻り値を検証
		assertEquals("index", indexResponse);
		//回数検証
		verify(todoService, times(1)).selectIncomplete();
		verify(todoService, times(1)).selectComplete();
		verify(todoService, times(0)).add(Mockito.any());
		verify(todoService, times(0)).update(Mockito.any());
		verify(todoService, times(0)).delete();

	}
	
	@Test
	public void add() {
		//リクエスト情報の設定
		Todo request = new Todo();
		request.setId(1);
		request.setTitle("リクエスト情報");
		request.setDone_flg(0);
		request.setTime_limit("2023-01-01");
		// テスト対象のメソッドを実行
		String response = todoController.add(request);
		// テスト対象の戻り値を検証
		assertEquals("redirect:/", response);
		//回数検証
		verify(todoService, times(0)).selectIncomplete();
		verify(todoService, times(0)).selectComplete();
		verify(todoService, times(1)).add(todoCaptor.capture());
		Todo todo = todoCaptor.getValue();
		assertEquals(todo,request);
		verify(todoService, times(0)).update(Mockito.any());
		verify(todoService, times(0)).delete();
	}
	
	@Test
	public void update() {
		//リクエスト情報の設定
		Todo addRequest = new Todo();
		addRequest.setId(1);
		addRequest.setTitle("リクエスト情報");
		addRequest.setDone_flg(0);
		addRequest.setTime_limit("2023-01-01");
		Todo updateRequest = new Todo();
		updateRequest.setId(1);
		updateRequest.setTitle("リクエスト");
		updateRequest.setDone_flg(1);
		updateRequest.setTime_limit("2035-01-01");
		// テスト対象のメソッドを実行
		todoController.update(updateRequest);
		// テスト対象の戻り値を検証
		assertEquals(true, todoController.flg);

		//回数検証
		verify(todoService, times(0)).selectIncomplete();
		verify(todoService, times(0)).selectComplete();
		verify(todoService, times(0)).add(Mockito.any());
		verify(todoService, times(1)).update(todoCaptor.capture());
		List<Todo> todoList = todoCaptor.getAllValues();
		assertEquals(todoList.get(0),updateRequest);
		verify(todoService, times(0)).delete();
	}
	
	@Test
	public void delete() {
		//リクエスト情報の設定
		// テスト対象のメソッドを実行
		todoController.delete();
		// テスト対象の戻り値を検証
		assertEquals(true, todoController.flg);
		//回数検証
		verify(todoService, times(0)).selectIncomplete();
		verify(todoService, times(0)).selectComplete();
		verify(todoService, times(0)).add(Mockito.any());
		verify(todoService, times(0)).update(Mockito.any());
		verify(todoService, times(1)).delete();
	}

	/**
	 * selectImcompleteのレスポンス
	 * 
	 * @return List<Todo>
	 */
	List<Todo> getSelectIncomplete() {
		List<Todo> todoExpect = new ArrayList<Todo>();
		Todo todo = new Todo();
		todo.setId(1);
		todo.setTitle("タスク1");
		todo.setDone_flg(0);
		todo.setTime_limit("2022-01-01");
		todoExpect.add(todo);
		return todoExpect;
	}

	/**
	 * selectCompleteのレスポンス
	 * 
	 * @return List<Todo>
	 */
	List<Todo> getSelectComplete() {
		List<Todo> todoExpect = new ArrayList<Todo>();
		Todo todo = new Todo();
		todo.setId(2);
		todo.setTitle("タスク2");
		todo.setDone_flg(1);
		todo.setTime_limit("2022-02-02");
		todoExpect.add(todo);
		return todoExpect;
	}
}
